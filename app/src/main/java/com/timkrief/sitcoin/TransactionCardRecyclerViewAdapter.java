package com.timkrief.sitcoin;

import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.timkrief.sitcoin.placeholder.PlaceholderContent.PlaceholderItem;
import com.timkrief.sitcoin.databinding.TransactionCardFragmentBinding;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PlaceholderItem}.
 * TODO: Replace the implementation with code for your data type.
 */
public class TransactionCardRecyclerViewAdapter extends RecyclerView.Adapter<TransactionCardRecyclerViewAdapter.ViewHolder> {

    private final List<Transaction> mTransactions;

    public TransactionCardRecyclerViewAdapter(List<Transaction> items) {
        mTransactions = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        Log.d("test","is working");
        return new ViewHolder(TransactionCardFragmentBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mTransactions.get(position);
        holder.mTitleView.setText(String.valueOf(holder.mItem.id));
        holder.mValueView.setText(String.valueOf(holder.mItem.value));
        holder.mStartDateView.setText(String.valueOf(holder.mItem.start));
        holder.mDurationView.setText(String.valueOf(holder.mItem.duration));
        holder.mEndDateView.setText(String.valueOf(holder.mItem.start + holder.mItem.duration));
    }

    @Override
    public int getItemCount() {
        return mTransactions.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mTitleView;
        public final TextView mValueView;
        public final TextView mStartDateView;
        public final TextView mEndDateView;
        public final TextView mDurationView;

        public Transaction mItem;

        public ViewHolder(TransactionCardFragmentBinding binding) {
            super(binding.getRoot());
            mTitleView = binding.titleLabel;
            mValueView = binding.valueLabel;
            mStartDateView = binding.startDateLabel;
            mEndDateView = binding.endDateLabel;
            mDurationView = binding.durationLabel;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mItem.toString() + "'";
        }
    }
}