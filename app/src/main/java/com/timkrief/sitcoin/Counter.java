/**
 *         SitCoin is a personal currency to balance the time you spend moving
 *         and the time you spend sitting.
 *         Copyright (C) 2021 Tim Krief
 *
 *         This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 *
 *         You should have received a copy of the GNU General Public License
 *         along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.timkrief.sitcoin;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.IBinder;

import androidx.core.app.NotificationCompat;
import androidx.preference.PreferenceManager;

import android.provider.AlarmClock;
import android.util.Log;

public class Counter extends Service {

    public static boolean running = false;

    private Handler handler;
    private float previousSitcoinAmount = 0;
    private Intent timer;
    private boolean alarmOn = false;

    private static final int NOTIFICATION_ID = 1;
    private NotificationCompat.BigTextStyle notificationStyle;
    private NotificationCompat.Builder notificationBuilder;

    private Bitmap coinBitmap;

    private PendingIntent contentPendingIntent;
    private PendingIntent pausePendingIntent;
    private PendingIntent earnPendingIntent;
    private PendingIntent spendPendingIntent;

    public static enum State {
        PAUSED,
        EARNING,
        SPENDING
    }

    public static final int ACTION_PAUSE = 1;
    public static final int ACTION_EARN = 2;
    public static final int ACTION_SPEND = 4;

    public State currentState = State.PAUSED;
    public int currentActions = 0;

    private void updateNotificationBuilder(State state, int actions) {
        if(state != currentState || actions != currentActions) {
            Log.v("CounterService","Notification builder updated");
            notificationBuilder = new NotificationCompat.Builder(this, "persistent")
                    .setContentIntent(contentPendingIntent)
                    .setAllowSystemGeneratedContextualActions(false)
                    .setLargeIcon(coinBitmap)
                    .setNotificationSilent()
                    .setOnlyAlertOnce(true)
                    .setOngoing(true)
                    .setShowWhen(false)
                    .setColorized(true)
                    .setStyle(notificationStyle);

            if (state == State.PAUSED) {
                notificationBuilder.setSmallIcon(R.drawable.notification_icon)
                        .setColor(getResources().getColor(R.color.pause_accent));
            } else if (state == State.EARNING) {
                notificationBuilder.setSmallIcon(R.drawable.notification_icon_earning)
                        .setColor(getResources().getColor(R.color.earn_accent));
            } else if (state == State.SPENDING) {
                notificationBuilder.setSmallIcon(R.drawable.notification_icon_spending)
                        .setColor(getResources().getColor(R.color.spend_accent));
            }

            if ((actions & ACTION_PAUSE) == ACTION_PAUSE) {
                notificationBuilder.addAction(R.drawable.notification_icon, "Pause", pausePendingIntent);
            }
            if ((actions & ACTION_EARN) == ACTION_EARN) {
                notificationBuilder.addAction(R.drawable.notification_icon_earning, "Earn", earnPendingIntent);
            }
            if ((actions & ACTION_SPEND) == ACTION_SPEND) {
                notificationBuilder.addAction(R.drawable.notification_icon_spending, "Spend", spendPendingIntent);
            }

            currentState = State.PAUSED;
            currentActions = actions;
        }
    }

    private void createNotification() {
        Log.v("CounterService","Notification created");

        Intent mainActivityIntent = new Intent(this, MainActivity.class);
        mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        contentPendingIntent = PendingIntent.getActivity(this, 0, mainActivityIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent pauseCounterIntent = new Intent(this, Counter.class);
        pauseCounterIntent.setAction("pause");
        pausePendingIntent = PendingIntent.getService(this, 0, pauseCounterIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent earnCounterIntent = new Intent(this, Counter.class);
        earnCounterIntent.setAction("earn");
        earnPendingIntent = PendingIntent.getService(this, 0, earnCounterIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent spendCounterIntent = new Intent(this, Counter.class);
        spendCounterIntent.setAction("spend");
        spendPendingIntent = PendingIntent.getService(this, 0, spendCounterIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        coinBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.coin);

        notificationStyle = new NotificationCompat.BigTextStyle();

        updateNotificationBuilder(State.PAUSED, ACTION_SPEND | ACTION_EARN);

        startForeground(NOTIFICATION_ID, notificationBuilder.build());
    }
    private void updateNotification() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean enableDebt = preferences.getBoolean("enableDebt", false);

        Log.v("CounterService","Notification updated");

        SharedPreferences sharedPref = getSharedPreferences("state", MODE_PRIVATE);

        float earningRate = sharedPref.getFloat("earningRate", 0.0f);

        float sitcoinAmount = SitCoinApplication.getSitcoinAmount();

        if(previousSitcoinAmount>0 && sitcoinAmount<=0) {
            alarmOn = true;
        }

        previousSitcoinAmount = sitcoinAmount;

        String title = "No SitCoin transaction";
        String description;

        if(sitcoinAmount>0) {
            updateNotificationBuilder(State.PAUSED, ACTION_SPEND | ACTION_EARN);
            description = "You have " + String.format("%.0f", sitcoinAmount) + " Sitcoins, feel free to spend them by sitting.";
        } else if(!enableDebt){
            updateNotificationBuilder(State.PAUSED, ACTION_EARN);
            description = "You have no more SitCoins, get moving to start earning some.";
        } else {
            updateNotificationBuilder(State.PAUSED, ACTION_SPEND | ACTION_EARN);
            description = "You are in debt, " + String.format("%.0f", sitcoinAmount) + " Sitcoins, get those SitCoins back!";
        }

        if(earningRate > 0) {
            updateNotificationBuilder(State.EARNING, ACTION_PAUSE | ACTION_SPEND);
            title = "By moving, you are earning SitCoins";
            description = "You have " + String.format("%.0f", sitcoinAmount) + " Sitcoins and counting! The more you move the more you earn.";
        }
        else if(earningRate < 0) {
            title = "By sitting, you are spending SitCoins";
            updateNotificationBuilder(State.SPENDING, ACTION_PAUSE | ACTION_EARN);
            if(!enableDebt){
                if(sitcoinAmount > 0) {
                    description = "You have " + String.format("%.0f", sitcoinAmount) + " Sitcoins left, each SitCoin is worth one minute of sitting.";
                } else {
                    sitcoinAmount = 0;
                }
            }
        }

        notificationStyle.bigText(description);
        notificationStyle.setBigContentTitle(title);
        notificationBuilder.setContentTitle(title);
        notificationStyle.setSummaryText("Balance: " + String.format("%.2f", sitcoinAmount) + " Sitcoins");

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v("CounterService","Service started");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(intent.getAction() != null && intent.getAction().equals("pause")){
            SitCoinApplication.TransactionStop();
        } else if(intent.getAction() != null && intent.getAction().equals("earn")) {
            SitCoinApplication.TransactionStartEarning();
        } else if(intent.getAction() != null && intent.getAction().equals("spend")) {
            SitCoinApplication.TransactionStartSpending();
        }
        if(!running) {
            createNotification();

            running = true;
            handler = new Handler();
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    updateNotification();
                    handler.postDelayed(this, 1000);
                    if (alarmOn) {
                        if(preferences.getBoolean("enableTimerAlert", true)) {
                            timer = new Intent(AlarmClock.ACTION_SET_TIMER);
                            timer.putExtra(AlarmClock.EXTRA_LENGTH, 1);
                            timer.putExtra(AlarmClock.EXTRA_MESSAGE, "SitCoin");
                            timer.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
                            timer.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(timer);
                        }
                        alarmOn = false;
                    }
                }
            };

            handler.post(runnable);
        }
        updateNotification();
        return Service.START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
