/**
 *         SitCoin is a personal currency to balance the time you spend moving
 *         and the time you spend sitting.
 *         Copyright (C) 2021 Tim Krief
 *
 *         This program is free software: you can redistribute it and/or modify
 *         it under the terms of the GNU General Public License as published by
 *         the Free Software Foundation, either version 3 of the License, or
 *         (at your option) any later version.
 *
 *         This program is distributed in the hope that it will be useful,
 *         but WITHOUT ANY WARRANTY; without even the implied warranty of
 *         MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *         GNU General Public License for more details.
 *
 *         You should have received a copy of the GNU General Public License
 *         along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.timkrief.sitcoin;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.preference.PreferenceManager;
import androidx.room.Room;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.timkrief.sitcoin.databinding.ActivityMainBinding;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    public float previousActionAmount;
    public long previousActionTime;
    public float earningRate;
    private float fakeSitcoinAmount;

    private ActivityMainBinding binding;

    FloatingActionButton fabEarn;
    FloatingActionButton fabSpend;
    TextView sitcoinUnits;
    TextView sitcoinDecimals;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_settings:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("persistent", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = this.getIntent();
        String data = intent.getDataString();
        if(data != null){
            if(data.split("\\.")[0].split("//")[1].equals("earn")){
                SitCoinApplication.TransactionStartEarning();
                finish();
            }
            if(data.split("\\.")[0].split("//")[1].equals("spend")){
                SitCoinApplication.TransactionStartSpending();
                finish();
            }
        }

        createNotificationChannel();
        if(!Counter.running) {
            Intent counterIntent = new Intent(this, Counter.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(counterIntent);
            } else {
                startService(counterIntent);
            }
        }

        SharedPreferences preference = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences sharedPref = getSharedPreferences("state", MODE_PRIVATE);

        previousActionAmount = sharedPref.getFloat("previousActionAmount", 0.0f);
        previousActionTime = sharedPref.getLong("previousActionTime", System.currentTimeMillis());
        earningRate = sharedPref.getFloat("earningRate", 0.0f);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());



        fabEarn = binding.fabEarn;
        fabSpend = binding.fabSpend;


        fabEarn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(earningRate > 0){
                    SitCoinApplication.TransactionStop();
                    fabEarn.setImageResource(R.drawable.notification_icon_earning);
                } else if(earningRate <= 0) {
                    SitCoinApplication.TransactionStartEarning();
                    fabEarn.setImageResource(android.R.drawable.ic_media_pause);
                    if(earningRate < 0) {
                        fabSpend.setImageResource(R.drawable.notification_icon_spending);
                    }
                }
                updateEarning();
            }
        });
        fabSpend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(earningRate < 0){
                    SitCoinApplication.TransactionStop();
                    fabSpend.setImageResource(R.drawable.notification_icon_spending);

                } else if(earningRate >= 0) {
                    SitCoinApplication.TransactionStartSpending();
                    fabSpend.setImageResource(android.R.drawable.ic_media_pause);
                    if(earningRate > 0) {
                        fabEarn.setImageResource(R.drawable.notification_icon_earning);
                    }
                }
                updateEarning();
            }
        });

        sitcoinUnits = binding.units;
        sitcoinDecimals = binding.decimals;
        fakeSitcoinAmount = SitCoinApplication.getSitcoinAmount();
        updateCoinLabel(fakeSitcoinAmount);

        final Handler handler = new Handler();

        handler.post(new Runnable() {
            @Override

            public void run()
            {
                if (earningRate != 0){
                    if(SitCoinApplication.getSitcoinAmount()<=0 && !preference.getBoolean("enableDebt", false)) {
                        fabSpend.setImageResource(R.drawable.notification_icon_spending);
                        fabEarn.setImageResource(R.drawable.notification_icon_earning);
                        SitCoinApplication.TransactionStop();
                    }
                }
                updateEarning();
                if (earningRate != 0) {
                    handler.postDelayed(this, (int) (600.f / Math.abs(earningRate)));
                } else {
                    handler.postDelayed(this, 500);
                }
            }
        });

    }
    private void updateCoinLabel(float amount){
        int units = (int) amount;
        sitcoinUnits.setText((Math.signum(amount)<0?"-":"") + Integer.toString(Math.abs(units)));

        int decimals = (int) (amount * 100) - units * 100;
        sitcoinDecimals.setText(String.format("%02d", Math.abs(decimals)));
    }

    public void updateEarning() {
        SharedPreferences sharedPref = getSharedPreferences("state", MODE_PRIVATE);

        previousActionAmount = sharedPref.getFloat("previousActionAmount", 0.0f);
        previousActionTime = sharedPref.getLong("previousActionTime", System.currentTimeMillis());
        earningRate = sharedPref.getFloat("earningRate", 0.0f);

        updateCoinLabel(SitCoinApplication.getSitcoinAmount());

        if(earningRate > 0) {
            fabEarn.setImageResource(android.R.drawable.ic_media_pause);
            fabSpend.setImageResource(R.drawable.notification_icon_spending);

            binding.currentCard.setBackgroundColor(getResources().getColor(R.color.earn_accent));
            binding.currentRateLabel.setBackgroundColor(getResources().getColor(R.color.earn_accent));
            String label = "+" + String.format("%.2f", earningRate);
            binding.currentRateLabel.setText(label);

            binding.currentActivityLabel.setText("Move");
            binding.currentActivityDescription.setText("Stand up and move");

            binding.currentRateLabel.setTextColor(getResources().getColor(R.color.black));
            binding.currentActivityLabel.setTextColor(getResources().getColor(R.color.black));
            binding.currentActivityDescription.setTextColor(getResources().getColor(R.color.black));
        } else if(earningRate < 0) {
            fabEarn.setImageResource(R.drawable.notification_icon_earning);
            fabSpend.setImageResource(android.R.drawable.ic_media_pause);

            binding.currentCard.setBackgroundColor(getResources().getColor(R.color.spend_accent));
            binding.currentRateLabel.setBackgroundColor(getResources().getColor(R.color.spend_accent));
            binding.currentRateLabel.setText(String.format("%.2f", earningRate));

            binding.currentActivityLabel.setText("Sit");
            binding.currentActivityDescription.setText("Sit down and rest");

            binding.currentRateLabel.setTextColor(getResources().getColor(R.color.white));
            binding.currentActivityLabel.setTextColor(getResources().getColor(R.color.white));
            binding.currentActivityDescription.setTextColor(getResources().getColor(R.color.white));
        } else {
            fabEarn.setImageResource(R.drawable.notification_icon_earning);
            fabSpend.setImageResource(R.drawable.notification_icon_spending);

            binding.currentCard.setBackgroundColor(getResources().getColor(R.color.pause_accent));
            binding.currentRateLabel.setBackgroundColor(getResources().getColor(R.color.pause_accent));
            binding.currentRateLabel.setText("0.00");

            binding.currentActivityLabel.setText("Pause");
            binding.currentActivityDescription.setText("Are you sleeping or something?");

            binding.currentRateLabel.setTextColor(getResources().getColor(R.color.white));
            binding.currentActivityLabel.setTextColor(getResources().getColor(R.color.white));
            binding.currentActivityDescription.setTextColor(getResources().getColor(R.color.white));
        }
    }
}

