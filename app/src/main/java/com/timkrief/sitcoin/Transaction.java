package com.timkrief.sitcoin;

import android.util.Log;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Transaction {
    @PrimaryKey(autoGenerate = true)
    public int id = 0;

    public Long start = System.currentTimeMillis();
    public Integer duration = -1;
    public Float value = 0.f;

    public String toString() {
        String result = "ID: " + Integer.toString(this.id) + ";\n";
        result += "Start: " + Long.toString(this.start) + ";\n";
        result += "Duration: " + Integer.toString(this.duration) + ";\n";
        result += "Value: " + Float.toString(this.value) + ";\n";

        return result;
    }
}
