package com.timkrief.sitcoin;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import androidx.preference.PreferenceManager;
import androidx.room.Room;

import java.util.List;

public class SitCoinApplication extends Application {

    private static Context context;

    public static List<Transaction> transactionList;

    public static void TransactionStop() {
        SitCoinApplication.setEarningRate(0);
    }

    public static void TransactionStartEarning() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Float moveEarningRate = preferences.getInt("earningRate",40)/Float.valueOf(preferences.getInt("earningRatePrecision",10));
        setEarningRate(moveEarningRate);
    }

    public static void TransactionStartSpending() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if(getSitcoinAmount()>0){
            setEarningRate(-1);
        } else if(preferences.getBoolean("enableDebt", true)) {
            setEarningRate(-1.f * preferences.getInt("debtRate",20)/Float.valueOf(preferences.getInt("debtRatePrecision",10)));
        }
    }

    public void onCreate() {
        super.onCreate();
        SitCoinApplication.context = getApplicationContext();

        AsyncTask.execute(() -> {
            AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                    AppDatabase.class, "database-name").build();

            TransactionDao transactionDao = db.transactionDao();

            Transaction newTransaction = new Transaction();

            transactionDao.insertAll(newTransaction);
            transactionList = transactionDao.getAll();
            for (Transaction transaction: transactionList) {
                Log.d("db:", transaction.toString());
            }
        });
    }

    public static Context getStaticApplicationContext() {
        return SitCoinApplication.context;
    }

    public static void setEarningRate(float rate) {
        long previousActionTime = System.currentTimeMillis();
        float previousActionAmount = getSitcoinAmount();

        SharedPreferences state = context.getSharedPreferences("state", MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = state.edit();

        stateEditor.putFloat("previousActionAmount", previousActionAmount);
        stateEditor.putLong("previousActionTime", previousActionTime);
        stateEditor.putFloat("earningRate", rate);
        stateEditor.apply();
    }

    public static float getEarningRate() {
        SharedPreferences state = context.getSharedPreferences("state", MODE_PRIVATE);
        return state.getFloat("earningRate", 0.0f);
    }

    public static float getSitcoinAmount() {
        long time = System.currentTimeMillis();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        boolean enableDebt = preferences.getBoolean("enableDebt", false);

        SharedPreferences state = context.getSharedPreferences("state", MODE_PRIVATE);

        float previousActionAmount = state.getFloat("previousActionAmount", 0.0f);
        long previousActionTime = state.getLong("previousActionTime", time);
        float earningRate = state.getFloat("earningRate", 0.0f);

        int elapsedTime = (int) (time - previousActionTime);
        if(elapsedTime < 0){
            elapsedTime = 0;
        }

        float sitcoinAmount = previousActionAmount + elapsedTime * earningRate / 60000.0f;

        if(sitcoinAmount < 0) {
            if (enableDebt) {
                if (previousActionAmount > 0) {
                    float debtRate = -1.f * preferences.getInt("debtRate",20)/Float.valueOf(preferences.getInt("debtRatePrecision",10));

                    long zeroActionTime = time - (long)(sitcoinAmount * 60000.0f / earningRate);

                    SharedPreferences.Editor stateEditor = state.edit();

                    stateEditor.putFloat("previousActionAmount", 0);
                    stateEditor.putLong("previousActionTime", zeroActionTime);
                    stateEditor.putFloat("earningRate", debtRate);
                    stateEditor.apply();

                    return getSitcoinAmount();
                }
            } else {
                return 0;
            }
        }
        return sitcoinAmount;
    }

    public static void setSitcoinAmount(float amount) {
        long previousActionTime = System.currentTimeMillis();
        SharedPreferences state = context.getSharedPreferences("state", MODE_PRIVATE);
        SharedPreferences.Editor stateEditor = state.edit();

        stateEditor.putFloat("previousActionAmount", amount);
        stateEditor.putLong("previousActionTime", previousActionTime);
        stateEditor.apply();
    }
}
