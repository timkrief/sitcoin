package com.timkrief.sitcoin;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.timkrief.sitcoin.databinding.FragmentOnboardingPageBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link OnboardingPageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OnboardingPageFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types of parameters
    private String mParam1;

    public OnboardingPageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment OnboardingPage1Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OnboardingPageFragment newInstance(String param1) {
        OnboardingPageFragment fragment = new OnboardingPageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        FragmentOnboardingPageBinding binding = FragmentOnboardingPageBinding.inflate(inflater, container, false);
        binding.mainText.setText(mParam1);

        return binding.getRoot();
    }
}