package com.timkrief.sitcoin;

import androidx.annotation.ColorInt;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.MarginPageTransformer;
import androidx.viewpager2.widget.ViewPager2;


import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.timkrief.sitcoin.databinding.ActivityOnboardingBinding;

public class OnboardingActivity extends AppCompatActivity {

    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static final int NUM_PAGES = 5;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager2 viewPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private FragmentStateAdapter pagerAdapter;

    ActivityOnboardingBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityOnboardingBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        binding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNextPressed();
            }
        });
        // Duplicate the dot



        for(int i=0; i<NUM_PAGES; i++){
            ImageView dot = new ImageView(this);
            dot.setImageResource(R.drawable.ic_baseline_circle_24);
            dot.setPadding(5,0,5,0);
            binding.progressDotsBar.addView(dot, 30,30);
        }

        // Instantiate a ViewPager2 and a PagerAdapter.
        viewPager = binding.pager;
        pagerAdapter = new ScreenSlidePagerAdapter(this);
        viewPager.setAdapter(pagerAdapter);

        viewPager.setPageTransformer(new MarginPageTransformer(200));
        viewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                if(position == NUM_PAGES - 1) {
                    binding.nextButton.setText(R.string.start);
                } else {
                    binding.nextButton.setText(R.string.next);
                }


                for(int i=0; i<NUM_PAGES; i++){
                    ImageView dot = (ImageView) binding.progressDotsBar.getChildAt(i);
                    if(i == position) {
                        dot.setAlpha(1.0f);
                    } else {
                        dot.setAlpha(0.3f);
                    }
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        if (viewPager.getCurrentItem() == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
            super.onBackPressed();
        } else {
            // Otherwise, select the previous step.
            viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
        }
    }

    public void onNextPressed() {
        if (viewPager.getCurrentItem() == NUM_PAGES - 1) {
            // If the user is currently looking at the last step, turn the next button into start
            Intent mainActivityIntent = new Intent(this, MainActivity.class);
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK |Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(mainActivityIntent);
        } else {
            // Otherwise, select the next step.
            viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
        }
    }

    /**
     * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStateAdapter {

        private TypedArray string_array;

        public ScreenSlidePagerAdapter(FragmentActivity fa) {
            super(fa);

            Resources res = getResources();
            string_array = res.obtainTypedArray(R.array.onboarding);
        }

        @Override
        public Fragment createFragment(int position) {
            return OnboardingPageFragment.newInstance(string_array.getString(position));
        }

        @Override
        public int getItemCount() {
            return NUM_PAGES;
        }
    }
}
