package com.timkrief.sitcoin;

import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.SeekBarPreference;
import androidx.preference.SwitchPreference;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.settings, new SettingsFragment())
                    .commit();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    public static class SettingsFragment extends PreferenceFragmentCompat {

        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.settings, rootKey);

            // - Value
            // -- totalValue
            EditTextPreference totalValue = this.findPreference("totalValue");

            if (totalValue != null) {
                totalValue.setOnBindEditTextListener(new EditTextPreference.OnBindEditTextListener() {
                    @Override
                    public void onBindEditText(@NonNull EditText editText) {
                        editText.setText(String.format("%.2f", SitCoinApplication.getSitcoinAmount()));
                        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                    }
                });

                totalValue.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        SitCoinApplication.setSitcoinAmount(Float.parseFloat(String.valueOf(newValue)));
                        return true;
                    }
                });
            }

            // -- earningRate
            updateSeekBar("earningRate", 1, 100, new SummaryUpdater() {
                @Override
                public String getString(Float value) {
                    return "You are earning " + value.toString() + " SitCoin" + (value != 1.0 ? "s" : "") + " every minute moving.";
                }
            });


            // - Alert
            // -- enableTimerAlert
            SwitchPreference enableTimerAlert = this.findPreference("enableTimerAlert");
            // -- enableDebt
            SwitchPreference enableDebt = this.findPreference("enableDebt");
            // -- debtRate
            updateSeekBar("debtRate", 10, 100, new SummaryUpdater() {
                @Override
                public String getString(Float value) {
                    if(value > 1.0f) {
                        return "The spending rate will be " + value.toString() + " times greater when in debt.";
                    } else {
                        return "No change to the spending rate when in debt.";
                    }
                }
            });

        }
        private interface SummaryUpdater{
            String getString (Float value);
        }
        private void updateSeekBar(String preferenceString, int min, int max, SummaryUpdater summaryUpdater){
            SeekBarPreference preference = this.findPreference(preferenceString);
            SeekBarPreference preferencePrecision = this.findPreference(preferenceString + "Precision");
            Float precision = Float.valueOf(preferencePrecision.getValue());
            Log.d("test", "updateSeekBar: " + precision.toString() );
            if (preference != null) {

                preference.setMin(min);
                preference.setMax(max);
                preference.setUpdatesContinuously(true);

                Float realValue = Float.parseFloat(String.valueOf(preference.getValue())) / precision;
                preference.setSummary(summaryUpdater.getString(realValue));

                preference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                    @Override
                    public boolean onPreferenceChange(Preference preference, Object newValue) {
                        Float realValue = Float.parseFloat(String.valueOf(newValue)) / precision;
                        preference.setSummary(summaryUpdater.getString(realValue));
                        return true;
                    }
                });
            }
        }
    }
}
