package com.timkrief.sitcoin;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
interface TransactionDao {
    @Query("SELECT * FROM `Transaction`")
    List<Transaction> getAll();

    @Insert
    void insertAll(Transaction... transactions);

    @Delete
    void delete(Transaction transaction);
}