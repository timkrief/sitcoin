# SitCoin

SitCoin is a personal currency to balance the time you spend moving and the time you spend sitting.

With this android app you can either earn coins by running a time while you are active or spend coins to sit.

## Contributing

This software is really young so I'll be mostly working on it on my own while it gets more mature.

At this stage, if you want to contribute, I would prefer you open an issue first to discuss what you would like to change.

## License
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
